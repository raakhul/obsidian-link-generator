import { MarkdownView,Editor, Plugin, TFile, Notice } from "obsidian";
import { LinkGeneratorSuggestModal } from "./modal";
//import { DataExtractorModal } from "./modal";

export default class LinkGeneratorPlugin extends Plugin {
  	async onload() {
		/* this.addCommand ( {
    	  id: "data-extractor",
    	  name: "Data Extractor",
    	  editorCallback: (editor: Editor) => {
    	    const selectedText = editor.getSelection();
		
    	    const onSubmit = (text: string, url: string) => {
			
    	    };
    	    //new DataExtractorModal(this.app, selectedText, onSubmit).open();
    	    //new DataExtractorSuggestModal(this.app, onSubmit).open();

    	  },
    	});*/
	
		this.addRibbonIcon("link", "Link Generator", async () => {    
			//const editor = this.app.workspace.getActiveViewOfType(MarkdownView)?.editor;
			const noteFile = this.app.workspace.getActiveFile(); // Currently Open Note
			if(!noteFile.name) return; // Nothing Open

			const headingsArray = await this.extractHeadings(noteFile);
			//new Notice(`Inside before sucess`);
		});
  	}

	onunload() {	
	}

  
	//To Read contents of active file
	async extractHeadings(noteFile: TFile):  Promise<any[]> {

		let headingsLinkArray: any;	
		// Read the currently open note file. We are reading it off the HDD - we are NOT accessing the editor to do this.
		let text = await this.app.vault.cachedRead(noteFile); //To read active cached file

		const onSubmit = (type: string, id: number, linktype: string) => {
			//new Notice(`inside onsubmit`);	
			let headingsArray:  RegExpMatchArray[];
			let extracted_headings: RegExpMatchArray;
			const editor = this.app.workspace.getActiveViewOfType(MarkdownView)?.editor;
			const selected=editor.getSelection();	
			if(linktype=='HP') {	
				if (id==2) {
					extracted_headings=text.match(/^##[\s].+/gm); //To select lines which has only heading 2	
					headingsArray = extracted_headings.map(headings => { //To Remove ## from selected headings
						let hashRemoved=headings.match(/(?![##\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==3) {
					extracted_headings=text.match(/^###[\s].+/gm); //To select lines which has only heading 3	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![###\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==4) {
					extracted_headings=text.match(/^####[\s].+/gm); //To select lines which has only heading 4	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![####\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==5) {
					extracted_headings=text.match(/^#####[\s].+/gm); //To select lines which has only heading 5	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![#####\s]).+/gm);
						return hashRemoved;
					})
				}

				headingsLinkArray=this.generateHeadingsLink(headingsArray); //To Generate md links for headings 2	
				editor.replaceSelection(headingsLinkArray.toString(),selected);

				return headingsLinkArray;
			}

			if(linktype=='HF') {
				let currentName = noteFile.name;
				let currentPath = noteFile.path;
				//new Notice(`inside HF currentname ${currentName} `);
				//new Notice(`inside HF currentpath ${currentPath} `);

				//let activePath = currentPath;
				//let currentPathSlash = currentPath.match(/\//gm).length; //Caculating no of slash from path of active file	
				if (id==6) {
					extracted_headings=text.match(/^##[\s].+/gm); //To select lines which has only heading 2	
					headingsArray = extracted_headings.map(headings => { //To Remove ## from selected headings
						let hashRemoved=headings.match(/(?![##\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==7) {
					extracted_headings=text.match(/^###[\s].+/gm); //To select lines which has only heading 3	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![###\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==8) {
					extracted_headings=text.match(/^####[\s].+/gm); //To select lines which has only heading 4	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![####\s]).+/gm);
						return hashRemoved;
					})
				}
				if (id==9) {
					extracted_headings=text.match(/^#####[\s].+/gm); //To select lines which has only heading 5	
					headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
						let hashRemoved=headings.match(/(?![#####\s]).+/gm);
						return hashRemoved;
					})
				}	
				headingsLinkArray=this.generateHeadingsLinkFullPath(headingsArray, currentPath); //To Generate md links for headings 2

				editor.replaceSelection(headingsLinkArray.toString(),selected);

				return headingsLinkArray;
			}	
			if(linktype=='BL') {
				// Read the currently open note file. We are reading it off the HDD - we are NOT accessing the editor to do this.
				let currentName = noteFile.name; //Active File Name
				let currentPath = noteFile.path; //Path of Active File
				let activePath = currentPath;
				let currentPathSlash = currentPath.match(/\//gm).length; //Caculating no of slash from path of active file 
				let finalFilePath = new Array();
				let filePath=this.app.vault.getMarkdownFiles().map((file) => file.path) //List of all markdown files	
				currentPath = currentPath.replace(currentName,"");
				currentPath = currentPath.replace("/","\/"); //Extracting the path before Active file	
				var regex = new RegExp(currentPath+".*"); //Generating regex from variable	
				//Extracting the filepath which comes under same level based on active file and no of /
				for (var i=0; i<filePath.length; i++) {
					if (regex.test(filePath[i])) {
						if ((currentPathSlash==filePath[i].match(/\//gm).length) && (activePath!==filePath[i])) {
							finalFilePath.push(filePath[i].toString())
						}
					}
				}
			
				let filenamesLinkArray=this.generateFilenamesLink(finalFilePath); //Generate md link for filenames under same path

				editor.replaceSelection(filenamesLinkArray.toString(),selected);
				return headingsLinkArray;
			}
			if(linktype=='CO') {
				extracted_headings=text.match(/^.+:/gm); //To select text with start with :
				//new Notice(`inside colon ${extracted_headings}`);

				headingsArray = extracted_headings.map(headings => { //To Remove ### from selected headings
					let hashRemoved=headings.match(/.+(?![:])./gm);
					return hashRemoved;
				})

				//new Notice(`inside colon ${headingsArray}`);

				let headingsArraynew = headingsArray.toString().split(",");
				new Notice(`inside colon ${headingsArraynew}`);
				for (var i=0; i<headingsArraynew.length;i++) {
					let updatedText: String;
					let originalText: String;
					new Notice(`extracted text ${headingsArraynew[i]}`);
					updatedText="_".concat(headingsArraynew[i].toString()).concat("_").concat(":");
					originalText=headingsArraynew[i].toString().concat(":");

					editor.replaceSelection(updatedText.toString(),originalText.toString());
					return headingsLinkArray;
				}
				
			}
			if(linktype=='HY') {

			}


		}	
		new LinkGeneratorSuggestModal(this.app, onSubmit).open();	
		return headingsLinkArray;
	}

  
  	//To Generate md links for headings
	generateHeadingsLink (headings: RegExpMatchArray[]): any[] {

		//new Notice(`Inside generate headings`);
		let headingsArray: Array<String>;
		let headingsLinkArray = new Array();

		headingsArray = headings.toString().split(",");

		for (var i=0; i<headingsArray.length;i++) {

			let heading: String;

			//Construting md link by replacing whitespace with /
			if (i==0) {
				heading="[".concat(headingsArray[i].toString()).concat("](#").concat(headingsArray[i].toString().toLowerCase().replace(/\s/g,"/")).concat(")");
				headingsLinkArray.push(heading.toString());
			}
			else {
				heading=" [".concat(headingsArray[i].toString()).concat("](#").concat(headingsArray[i].toString().toLowerCase().replace(/\s/g,"/")).concat(")");
				headingsLinkArray.push(heading.toString());
			}

			/*
			if (i>=1 && i<(headingsArray.length-1)) {
				heading=" [".concat(headingsArray[i].toString()).concat("](#").concat(headingsArray[i].toString().toLowerCase().replace(/\s/g,"/")).concat(")");
				headingsLinkArray.push(heading.toString());
			}*/
		}

		return headingsLinkArray;
	}

  	//To Generate md links for headings with full path
	generateHeadingsLinkFullPath (headings: RegExpMatchArray[], currentPath:String ): any[] {

		//new Notice(`Inside generate headings`);
		let headingsArray: Array<String>;
		let headingsLinkArray = new Array();

		headingsArray = headings.toString().split(",");

		for (var i=0; i<headingsArray.length;i++) {

			let heading: String;

			//Construting md link by replacing whitespace with /
			if (i==0) {
				heading="[".concat(headingsArray[i].toString()).concat("](").concat(currentPath.toString().replace(/\s/g,"%20")).concat("#").concat(headingsArray[i].toString().toLowerCase().replace(/\s/g,"/")).concat(")");
				headingsLinkArray.push(heading.toString());
			}
			else {
				heading=" [".concat(headingsArray[i].toString()).concat("](").concat(currentPath.toString().replace(/\s/g,"%20")).concat("#").concat(headingsArray[i].toString().toLowerCase().replace(/\s/g,"/")).concat(")");
				headingsLinkArray.push(heading.toString());
			}
		}

		return headingsLinkArray;
	}

	//Generate md link for filenames under same path
	generateFilenamesLink (filenames: any[]): any[] {

		let filenamesArray: Array<String>;
		let filenamesLinkArray = new Array();
		filenamesArray = filenames.toString().split(",");
		

		for (var i=0; i<filenamesArray.length;i++) {
			let filename: String;

			//Selecting the other filenames from file path
			let matchedPattern=(filenamesArray[i].match(/[\w-\s]+\.../g)).toString(); 
			matchedPattern=matchedPattern.replace(".md",""); //Removing .md extension

			//Constructing md links for filenames under same level
			if (i==0) {
				filename="[".concat(matchedPattern.toString())
				filename=filename.concat("](").concat(filenamesArray[i].toString().replace(/\s/g,"%20")).concat(")");
				filenamesLinkArray.push(filename.toString());
			}
			else {
				filename=" [".concat(matchedPattern.toString()).concat("](").concat(filenamesArray[i].toString().replace(/\s/g,"%20")).concat(")");
				filenamesLinkArray.push(filename.toString());
			}
		}

		filenamesLinkArray=filenamesLinkArray.sort();

		return filenamesLinkArray;
	}

	
}