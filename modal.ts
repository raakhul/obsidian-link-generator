import { MarkdownView, App, Modal, Notice, Setting, SuggestModal } from "obsidian";

interface ExtractType {
  type: string;
  id: number;
  linktype: string;
}

const ALL_TYPES = [
  {
    type: "Headings 2",
    id: 2,
    linktype: "HP",
  },
  {
    type: "Headings 3",
    id: 3,
    linktype: "HP",
  },
  {
    type: "Headings 4",
    id: 4,
    linktype: "HP",
  },
  {
    type: "Headings 5",
    id: 5,
    linktype: "HP",
  },
  {
    type: "Headings 2 Full Path",
    id: 6,
    linktype: "HF",
  },
  {
    type: "Headings 3 Full Path",
    id: 7,
    linktype: "HF",
  },
  {
    type: "Headings 4 Full Path",
    id: 8,
    linktype: "HF",
  },
  {
    type: "Headings 5 Full Path",
    id: 9,
    linktype: "HF",
  },
  {
    type: "Backlinks within Same Level",
    id: 10,
    linktype: "BL",
  },
  {
    type: "Bold Font before Colon",
    id: 11,
    linktype: "CO",
  },
  {
    type: "Bold Font before Hyphen",
    id: 12,
    linktype: "HY",
  },
  {
    type: "Italics Font before Colon",
    id: 13,
    linktype: "CO",
  },
  {
    type: "Italics Font before Hyphen",
    id: 14,
    linktype: "HY",
  },

];

export class LinkGeneratorSuggestModal extends SuggestModal<ExtractType> {

  type: string;
  id: number;
  linktype: string;

  onSubmit: (type: string, id: number, linktype: string) => void;

  constructor(
    app: App,
    //defaultLinkText: string,
    onSubmit: (type: string, id: number, linktype: string) => void
  ) {
    super(app);
    //this.type = defaultLinkText;
    this.onSubmit = onSubmit;
  }

  // Returns all available suggestions.
  getSuggestions(query: string): ExtractType[] {
    return ALL_TYPES.filter((extracttype) =>
    extracttype.type.toLowerCase().includes(query.toLowerCase())
    );
  }

  // Renders each suggestion item.
  renderSuggestion(extracttype: ExtractType, el: HTMLElement) {
    el.createEl("div", { text: extracttype.type });
    //el.createEl("small", { textContent: extracttype.id });
  }

  // Perform action on the selected suggestion.
  onChooseSuggestion(extracttype: ExtractType, evt: MouseEvent | KeyboardEvent) {
    //new Notice(`Selected onChooseSuggestion ${extracttype.type} ${evt} `);
    this.type=extracttype.type;
    this.id=extracttype.id;
    this.linktype=extracttype.linktype;
    //new Notice(`Before Return`);
    return this.onSubmit(this.type, this.id, this.linktype);
  }


}


